#pragma once

#include <QWidget>
#include <QLabel>
#include <pwm_value.h>
#include "pca9685.h"

// From pca9685.h
// #define PCA9685_CHANNELS 16

class PWM_Device : public QWidget {

    Q_OBJECT
    
    public:
        PWM_Device(QWidget *parent = 0, int index = 0, QString name = "<<<Device Label>>>");
        void get_values(int* result);
        void push_values(PCA9685* pca_dev);
    
    private:
        QLabel* label;
        PWM_Value* pwm_values[PCA9685_CHANNELS];
        int idx;
        QString name;
};
#include <QVboxLayout>
#include <QString>
#include "pwm_device.h"
#include "pwm_value.h"
#include "pca9685.h"

PWM_Device::PWM_Device(QWidget *parent, int index, QString name) : QWidget(parent), idx(index), name(name) {
    
    QVBoxLayout *vbox = new QVBoxLayout(this);

    // Text label for the device name
    label = new QLabel(this);
    label->setText(name);
    vbox->addWidget(label);

    // Create a PWM value slider/text box for each channel on this device.
    for(int i=0; i<PCA9685_CHANNELS; i++) {
        pwm_values[i] = new PWM_Value(this, i);
        vbox->addWidget(pwm_values[i]);
    }

    // Set some spacing rules. Don't allow stretching vertically (looks better this way in my opinion)
    vbox->setSpacing(3);
    vbox->setContentsMargins(3,3,3,3);
    vbox->addStretch();

}

// Loop through all the channels and store the value into the pointed to array.
void PWM_Device::get_values(int* result) {
    for(int i=0; i<PCA9685_CHANNELS; i++) {
        result[i] = pwm_values[i]->get_value();
    }
}

// Pushes the values of the channels onto the PCA9685 device object.
void PWM_Device::push_values(PCA9685* pca_dev) {
    int res[PCA9685_CHANNELS];
    this->get_values(res);
    for(int i=0; i<PCA9685_CHANNELS; i++) {
        pca_dev->PCA_setpwm(i, res[i]);
    }
}
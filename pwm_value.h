#pragma once

#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QSpinBox>

#define LABEL_SIZE 60

class PWM_Value : public QWidget {

    Q_OBJECT
    
    public:
        PWM_Value(QWidget *parent = 0, int index = 0);
        int get_value();
    
    private:
        QLabel* label;
        QSlider* slider;
        QSpinBox* spinbox;
        int idx;
};
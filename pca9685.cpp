#include "pca9685.h"
#include <stdio.h>

// Class constructor.
PCA9685::PCA9685(int dev_num, uint8_t addr) : device_num(dev_num), address(addr) {
    PCA_reset();
}

// Reset function, to set PWMs to 0 and go back to a basic state.
void PCA9685::PCA_reset() {
    for(int i=0; i<PCA9685_CHANNELS; i++) {
        pwm_val[i] = 0;
        on_count[i] = i * PCA9685_COUNTER_WIDTH / PCA9685_CHANNELS;
        off_count[i] = PCA9685_ALL_ONOFF_BIT;
    }
}

// Sets the PWM value of the specified channel to the specified percentage.
// Percentages should be bound to between 0 and 100.
// Set the updated flag (take 1, bit shift by whichever channel it is to select the right index).
void PCA9685::PCA_setpwm(int channel, int pwm) {
    int old_pwm = pwm_val[channel];
    if(pwm >= 100) {
        pwm_val[channel] = 100;
    } else if(pwm <= 0) {
        pwm_val[channel] = 0;
    } else {
        pwm_val[channel] = pwm;
        int pwm_width = pwm * PCA9685_COUNTER_WIDTH / 100;
        off_count[channel] = (on_count[channel] + pwm_width) % PCA9685_COUNTER_WIDTH;
    }
    if(pwm_val[channel] != old_pwm) {
        channel_updated = channel_updated | (0x0001 << channel);
    }
}

// To reset the channel updated flags after writing, just set to 0.
void PCA9685::PCA_clear_update_flag() {
    channel_updated = 0;
}

void PCA9685::PCA_print_status() {
    printf("Device %d PWM val | ON ct | OFF ct | upd\n", device_num);
    for(int i=0; i<PCA9685_CHANNELS; i++) {
        int upd = (channel_updated >> i) % 2;
        printf("    Ch.%2d:   %3d |  %4d |   %4d |   %d\n", i, pwm_val[i], on_count[i], off_count[i], upd);
    }
}

uint8_t PCA9685::get_address() {
    return this->address;
}

uint16_t PCA9685::get_channel_updated() {
    return this->channel_updated;
}

uint16_t PCA9685::get_on_count(int idx) {
    return this->on_count[idx];
}

uint16_t PCA9685::get_off_count(int idx) {
    return this->off_count[idx];
}

int PCA9685::get_pwm_val(int idx) {
    return pwm_val[idx];
}
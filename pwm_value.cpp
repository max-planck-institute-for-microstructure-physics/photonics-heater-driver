#include <QHboxLayout>
#include <QString>
#include "pwm_value.h"

PWM_Value::PWM_Value(QWidget *parent, int index) : QWidget(parent), idx(index) {
    
    QHBoxLayout *hbox = new QHBoxLayout(this);
    
    // Text label for which channel number this is.
    label = new QLabel(this);
    label->setText(QStringLiteral("Channel %1:").arg(idx));
    label->setAlignment(Qt::AlignRight);
    label->setMinimumSize(LABEL_SIZE, 0);
    hbox->addWidget(label);
    
    // Create slider and spinbox (with no arrows) for setting the value.
    // Users can either use the slider, mouse scroll in the box, or type a value.
    slider = new QSlider(Qt::Horizontal, this);
    slider->setMinimum(0);
    slider->setMaximum(100);
    hbox->addWidget(slider);
    spinbox = new QSpinBox(this);
    spinbox->setMinimum(0);
    spinbox->setMaximum(100);
    spinbox->setButtonSymbols(QAbstractSpinBox::NoButtons);
    spinbox->setSuffix("%");
    hbox->addWidget(spinbox);
    
    // Set some alignment and spacing rules
    hbox->setSpacing(3);
    hbox->setContentsMargins(3,3,3,3);
    hbox->setAlignment(label, Qt::AlignVCenter);
    hbox->setAlignment(slider, Qt::AlignVCenter);
    hbox->setAlignment(spinbox, Qt::AlignVCenter);
    
    // Set up connections to sync the value of the slider and the spinbox. When the value of one changes,
    // can automatically change the other.
    connect(slider, &QSlider::valueChanged, spinbox, &QSpinBox::setValue);
    connect(spinbox, QOverload<int>::of(&QSpinBox::valueChanged), slider, &QSlider::setValue);
    
}

// Accessor function
int PWM_Value::get_value() {
    return slider->value();
}
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include "MCP2221A.h"

#include "mcp2221_dll_um.h"

/* Default addresses per the datasheet */
#define VID 0x4d8
#define PID 0xdd

int MCP_init(void* device){

    // Library Version
    wchar_t libVer;
    MCP_checkLibrary(&libVer);

    // Discover Devices
    MCP_getDevices();

    // Open device
    device = Mcp2221_OpenByIndex(VID, PID, 0);
    int error = Mcp2221_GetLastError();
    if(error != E_ERR_INVALID_HANDLE){
        printf("Connection successful\n");
        return 0;
    }
    else{
        printf("Error message is %d\n", error);
        return -1;
    }
}

int MCP_checkLibrary(wchar_t* libVer){

    int rVer = Mcp2221_GetLibraryVersion(libVer);
    if (rVer==0){
        printf("Library Version: %ls\n", libVer);
        return 0;
    }
    else{
        int error = Mcp2221_GetLastError();
        printf("Version not found, error: %d\n",  error);
        return -1;
    }
}
int MCP_getDevices(void){
    //TODO: VID,PID
    unsigned int NumOfDev;
    Mcp2221_GetConnectedDevices(VID, PID, &NumOfDev);
    if(NumOfDev == 0){
        printf("No MCP2221 devices connected\n");
    }
    else{
        printf("Number of devices found: %d\n", NumOfDev);
    }
    return 0;
}
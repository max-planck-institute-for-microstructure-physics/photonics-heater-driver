#include <QHboxLayout>
#include <QVboxLayout>
#include <QPushButton>
#include <iostream>
#include <stdlib.h>
#include "pwm_device.h"
#include "opa_wrapper.h"
#include "pca9685.h"
#include "mcp2221_dll_um.h"

#define VID 0x4d8
#define PID 0xdd

OPA_Wrapper::OPA_Wrapper(QWidget *parent) : QWidget(parent) {
    
    // ******************************************
    // Hardware setup
    // ******************************************
    get_addresses_from_file("addTable.txt");
    for(int i=0; i<N_DEVICES; i++) {
        pca_dev[i] = PCA9685(i, addresses[i]);
    }
    if(MCP_init()) {
        abort();
    }
    MCP_PCA_init();
    //PCA_regI2cDriver(&pcaDriver, &mcp_dev, N_DEVICES);
    //printf("ok.... \n");
    
    // ******************************************
    // UI Layout
    // ******************************************
    hbox = new QHBoxLayout();
    vbox = new QVBoxLayout(this);
    
    // Create a channel for each device. Get the label name from the enumerated strings.
    for(int i=0; i<N_DEVICES; i++) {
        pwm_devices[i] = new PWM_Device(this, i, DEVICE_STRINGS[i] + " (0x" + QString::number(addresses[i], 16) + ")");
        hbox->addWidget(pwm_devices[i]);
        
        // Between each device, add a spacer.
        if(i<N_DEVICES-1){
            spacer[i] = new QWidget(this);
            spacer[i]->setFixedWidth(1);
            spacer[i]->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
            spacer[i]->setStyleSheet(QString("background-color: #c0c0c0;"));
            hbox->addWidget(spacer[i]);
        }
    }

    // Create a vertical box, which will have the push-button for sending data to the device,
    // as well as the above created hbox with all the devices.

    button = new QPushButton("Send Data", this);
    button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    vspacer = new QWidget(this);
    vspacer->setFixedHeight(2);
    vspacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    vspacer->setStyleSheet(QString("background-color: #c0c0c0;"));

    vbox->addLayout(hbox, 0);
    vbox->addWidget(vspacer);
    vbox->addWidget(button);
    vbox->addStretch();
    
    setLayout(vbox);
    
    connect(button, &QPushButton::released, this, &OPA_Wrapper::push_all_values);

}

// Debug function, for printing the values out to console without writing to the hardware
void OPA_Wrapper::print_results() {
    for(int i=0; i<N_DEVICES; i++) {
        pca_dev[i].PCA_print_status();
    }
}

// Gets the values from the lower level widgets, and writes out to the hardware.
void OPA_Wrapper::push_all_values() {
    for(int i=0; i<N_DEVICES; i++) {
        pwm_devices[i]->push_values(&this->pca_dev[i]);
        this->MCP_PCA_pwm_values_push(i);
    }
    //this->print_results();
}

// Initializes the connection to the MCP device
int OPA_Wrapper::MCP_init() {

    // Library Version
    wchar_t libVer;
    MCP_checkLibrary(&libVer);

    // Discover Devices
    MCP_getDevices();

    // Open device
    this->mcp_dev = Mcp2221_OpenByIndex(VID, PID, 0);
    int error = Mcp2221_GetLastError();
    printf("error is: %d\n", error);
    if(error == 0){
        printf("Connection successful\n");
        return 0;
    }
    else{
        printf("Error message is %d\n", error);
        return -1;
    }
}

// Helper function to ensure that the library is loaded properly.
int OPA_Wrapper::MCP_checkLibrary(wchar_t* libVer) {

    int rVer = Mcp2221_GetLibraryVersion(libVer);
    if (rVer==0){
        printf("Library Version: %ls\n", libVer);
        return 0;
    }
    else{
        int error = Mcp2221_GetLastError();
        printf("Version not found, error: %d\n",  error);
        return -1;
    }
}

// Enumerates the MCP2221A devices on USB
void OPA_Wrapper::MCP_getDevices() {
    //TODO: VID,PID
    unsigned int NumOfDev;
    Mcp2221_GetConnectedDevices(VID, PID, &NumOfDev);
    if(NumOfDev == 0){
        printf("No MCP2221 devices connected\n");
    }
    else{
        printf("Number of devices found: %d\n", NumOfDev);
    }
}

// Loads the I2C addresses from the file into the addresses array.
void OPA_Wrapper::get_addresses_from_file(const char* path) {
    FILE* filein = fopen(path, "r");
    int num = 0;
    int i = 0;
    
    while(fscanf(filein, "%x", &num)) {
        if(i<N_DEVICES) {
            addresses[i] = num;
            i++;
        } else {
            break;
        }
    }
}

// Hardware initialization function, to send the initial values over the MCP2221A chip to each device.
// Initialization instructions in opa-driver-software.md
void OPA_Wrapper::MCP_PCA_init() {
    unsigned char write_data[1];
    for(int i=0; i<N_DEVICES; i++) {
        write_data[0] = {0b00000011};
        this->MCP_write(i, PCA9685_PRESCALE_REG, 1, write_data);
        
        write_data[0] = {0b00000110};
        this->MCP_write(i, PCA9685_MODE2_REG, 1, write_data);
        
        write_data[0] = {0b00000001};
        this->MCP_write(i, PCA9685_MODE1_REG, 1, write_data);
    }
}

// Writes data to the PCA9685 hardware using the MCP2221A device
void OPA_Wrapper::MCP_write(int device_num, unsigned char register_address, unsigned int num_bytes, unsigned char* data) {
    const char use_7bit_address = 0x0;
    unsigned char slave_address = (unsigned char) this->pca_dev[device_num].get_address();
    unsigned char tx_data[num_bytes+1];
    tx_data[0] = register_address;
    for(int i=0; i<num_bytes; i++) {
        tx_data[i+1] = data[i];
    }
    int res = Mcp2221_I2cWrite(this->mcp_dev, num_bytes+1, slave_address, use_7bit_address, tx_data);
    if(res != 0) {
        printf("Error writing to device %d, register address %2X\n", device_num, register_address);
        abort();
    }
}

// Writes out the PWM values to hardware.
void OPA_Wrapper::MCP_PCA_pwm_values_push(int device_num) {
    uint16_t upd = pca_dev[device_num].get_channel_updated();
    uint16_t on_count_i = 0;
    uint16_t off_count_i = 0;
    unsigned char reg_addr;
    const unsigned char num_bytes = 1;
    unsigned char write_data[num_bytes] = {0};
    int pwm_val = 0;
    for(int i=0; i<PCA9685_CHANNELS; i++) {
        if(upd%2) {
            printf("Attempting to write...\n");
            pwm_val = pca_dev[device_num].get_pwm_val(i);
            
            if (pwm_val == 100) {
                printf("LEDs set to FULL ON\n");
                on_count_i = PCA9685_LED_ON_FULL;
                write_data[0] = get_lsbyte(on_count_i);
                reg_addr = PCA9685_LED_ON_L_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
            
                write_data[0] = get_msbyte(on_count_i);
                reg_addr = PCA9685_LED_ON_H_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
                
                off_count_i = PCA9685_LED_OFF_FULL;
                write_data[0] = get_lsbyte(off_count_i);
                reg_addr = PCA9685_LED_OFF_L_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
            
                write_data[0] = get_msbyte(off_count_i);
                reg_addr = PCA9685_LED_OFF_H_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
            } else if (pwm_val == 0) {
                printf("LEDs set to FULL OFF\n");
                on_count_i = PCA9685_LED_ON_ZERO;
                write_data[0] = get_lsbyte(on_count_i);
                reg_addr = PCA9685_LED_ON_L_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
            
                write_data[0] = get_msbyte(on_count_i);
                reg_addr = PCA9685_LED_ON_H_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
                
                off_count_i = PCA9685_LED_OFF_ZERO;
                write_data[0] = get_lsbyte(off_count_i);
                reg_addr = PCA9685_LED_OFF_L_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
            
                write_data[0] = get_msbyte(off_count_i);
                reg_addr = PCA9685_LED_OFF_H_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
            } else {
                printf("LEDs set to specific values\n");
                on_count_i = pca_dev[device_num].get_on_count(i);
                write_data[0] = get_lsbyte(on_count_i);
                reg_addr = PCA9685_LED_ON_L_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
                printf("Wrote ON Low\n");
            
                write_data[0] = get_msbyte(on_count_i);
                reg_addr = PCA9685_LED_ON_H_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
                printf("Wrote ON High\n");
            
                off_count_i = pca_dev[device_num].get_off_count(i);
                write_data[0] = get_lsbyte(off_count_i);
                reg_addr = PCA9685_LED_OFF_L_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
                printf("Wrote OFF Low\n");
            
                write_data[0] = get_msbyte(off_count_i);
                reg_addr = PCA9685_LED_OFF_H_BASE + PCA9685_LED_INCR * i;
                this->MCP_write(device_num, reg_addr, num_bytes, write_data);
                printf("Wrote OFF High\n");
            }
        }
        upd = upd >> 1;
    }
    this->pca_dev[device_num].PCA_clear_update_flag();
}

// Little-endian only - gets the lower byte of the 16-bit value.
unsigned char get_lsbyte(uint16_t in) {
    return (unsigned char) (in & 0x00FF);
}

// Little-endian only - gets the upper byte of the 16-bit value.
unsigned char get_msbyte(uint16_t in) {
    return (unsigned char) ((in & 0xFF00) >> 8);
}
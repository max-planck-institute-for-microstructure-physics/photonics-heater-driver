# Driver Software for OPA Testing

## Overview

The OPA_BASE board block diagram is shown below:

Green boxes indicate connectors off-board. Black boxes are integrated circuits on the board.

There are two possible paths for data: Over USB, through the MCP2221A chip, to I2C, or directly as I2C from an FPGA. The I2C interface programs PWM values in the PCA9685 chips. These PWMs drive the gate of a transistor driver that puts 15 V (for the optical phased arrays) or 24 V (for the MEMS cantilevers) on the chip. There are also a set of 10 test points which can be used for measurement and verification.

The software interface is designed to be run from a Windows computer, with a GUI to set the PWM duty cycle as a percent. This will allow photonics designers to easily test their devices.

Credit to summer student Jordan Hong (University of Toronto) for writing the base code. The git repo is found at: <https://github.com/jordanhong/pca9685-c>

This code has been modified to C++ in order to operate with the Qt GUI framework, to allow users to set values more easily.

## Quick Start

### Hardware setup

You will require the OPA_BASE board, and the carrier board for whichever device is being measured (OPA_MEMS_A/B, OPA_AB_A/B, OPA_C_A/B). The A/B revs refer to the specific set of pins on the chip which are connected on the boards.

1. Ensure that the jumpers on the OPA_BASE are correctly set: For each of the 3 jumpers, the middle pin is the common pin which connects to the hardware, the left pin is for USB, and the right pin is for FPGA. Set the jumpers to USB mode:
2. Install hardware on test bench using the M6 screw holes available on the boards.
3. Use the ribbon cable connectors to connect the conncetors labeled `DEVICE N` on the OPA_BASE board to its corresponding `DEVICE N` on the carrier board.
4. Connect the power supplies to the +24V, +15V, and their corresponding GND pins.
5. Plug the OPA_BASE into USB.

A screenshot of the windows software for controlling the devices is shown below.

![](https://keeper.mpdl.mpg.de/lib/b2fd0bbd-8489-4b0e-a001-51c1580d25f1/file/images/auto-upload/image-1606844220217.png?raw=1)

Operation is simple: set the slider for the desired channel to the desired PWM percentage value, then press the "Send data" button to send to the device.

### Building the project

Install MSYS2 and MINGW32 and MINGW64. Install Qt development files to the MSYS2 installation. Download the source code from the git repository.

Run `qmake`  to set up to make the Qt project

Run `mingw32-make.exe`  to build as portable

Run `windeployqt.exe release/qt-test.exe` to get the required libraries and drivers

Release the entire `release`  directory.

## Code Overview

### Directory Structure

### File description

Comments for all functions are found in the respective `.cpp` files, and some below in the "Classes" section of this document. Some additional files of interest are shown below:

* `addTable.txt`: Address table for the I2C devices. Each line corresponds to a hex value of the device labeled with the line number, the MEMS device is considered device 7.

### Classes

#### PWM_Value

QWidget based class for setting the PWM value using a slider or text entry box. The slider is constrained to integer values from 0 to 100 inclusive, allowing the PWM to be set within that range.

Public functions:

* `PWM_Value::get_value()`: returns the value of the slider.

#### PWM_Device

QWidget based class for arranging a set of the above PWM_Value objects, one for each "channel" on the "device".

Public functions:

* `PWM_Device::get_values(int* result)`: Gets the value for each of the channels and returns into the array pointed to by `int* result`.
* `PWM_Device::push_values(PCA9685* pca_dev)`: Pushes the values of each channel onto the PCA9685 object (note that this does not write to hardware - just stores the values in a buffer to be written).

#### OPA_Wrapper

QWidget based class for arranging a set of the above PWM_Device objects, one for each "device" in the system. Holds an array of PWM_Device objects, an array of PCA9685 objects, and the handle to the MCP2221A device. There is also a push-button, which can be used to write the data to hardware.

Public functions:

* `inline const QString DEVICE_STRINGS[N_DEVICES]`: Array of strings containing the labels of the devices.
* `OPA_Wrapper::print_results()`: A debug function for writing values out to console, without writing to the hardware.
* `OPA_Wrapper::push_all_values()`: Top level wrapper function that calls the relevant functions required to pull data from the PWM_Value devices all the way through to pushing out to hardware.
* `OPA_Wrapper::MCP_init()`: initializes a connection to the MCP2221A device, sets up all the internal objects and pointers required for interfacing with the DLL.
* `OPA_Wrapper::MCP_checkLibrary(wchar_t* libVer)`: Helper function to check the library version of the MCP2221A.
* `OPA_Wrapper::MCP_getDevices()`: Helper function to enumerate the connected MCP2221A devices on USB. Currently relies on some default values for VID and PID values, which maybe can be programmatically determined. Should be ok to just use the defaults, as only one base board is likely to be connected at a time.
* `OPA_Wrapper::MCP_PCA_init()`: Initializes the hardware by sending the correct MODE settings to the PCA9685 devices (see below in the "PCA9685 Functional Description" section for some details).

Private functions:

* `OPA_Wrapper::get_addresses_from_file(const char* path)`: Gets the file with the I2C addresses of each device. This file is one value per line, in hex, each line corresponding to the devices in order. Default file name is "addTable.txt".
* `OPA_Wrapper::MCP_write(int device_num, unsigned char register_address, unsigned int num_bytes, unsigned char* data)`: Writes the array (size `num_bytes`) of bytes in `data` to the register with address `register_address` on the PCA9685 device indicated by `device_num`.
* `OPA_Wrapper::MCP_PCA_pwm_values_push(int device_num)`: Helper wrapper function to grab the data from the PCA9685 objects (after pushing values to it using `PWM_Device::push_values`), and write it to hardware. This function could currently stand to have some clean up I think - but it writes the data one byte at a time, intelligently determining the settings based on the PWM value, as 100% on and 0% on have special settings.

Other functions:

* `get_lsbyte(uint16_t in)`: Gets the least significant 8 bits of the input value. Not written to support anything except little endian systems.
* `get_msbyte(uint16_t in)`: Gets the most significant 8 bits of the input value. Not written to support anything except little endian systems.

#### PCA9685

Abstract object holding relevant values for the PCA9685 hardware. See the `PCA9685 Functional Description` section for some more details about how the hardware works. Basic summary is, there is an index holding which device number the object is (`device_num`), a byte for the address (`address`), an array holding the PWM value (`pwm_val`, as a percent, integer from 0 to 100, indexed by channel), an array holding the "on count" which is the value of the free-running counter on which the PWM rising edge occurs (`on_count`, indexed by channel), an array holding the "off count" which is the value of the free-running counter on which the PWM falling edge occurs (`off_count`, indexed by channel), and a value tracking whether the each channel has been updated (`channel_updated`, bit-indexed by channel).

The header file for this class also has all of the addresses as `#define`s for the registers in the PCA9685 hardware device.

Public functions:

* `PCA9685::PCA_reset()`: resets the values stored in the device to their defaults. The `on_count` values are pre-determined, in order to ensure that there isn't a sudden burst of current when all of the devices turn on at once, and the `off_count` value is set to have each channel be in the always off state.
* `PCA9685::PCA_setpwm(int channel, int pwm)`: Sets the PWM value `pwm` into the channel indexed by `channel`. Checks to ensure that the values are bounded by 0 and 100. If the value has changed, we update the `channel_updated` flag for that channel. We also calculate the turn-off time here (see `PCA9685 Functional Description` section for calculations).
* `PCA9685::PCA_clear_update_flag()`: Clears the `channel_updated` value after writing.
* `PCA9685::PCA_print_status()`: Debug function to print the values in the device.
* `PCA9685::get_address()`, `PCA9685::get_channel_updated()`, `PCA9685::get_on_count(int idx)`, `PCA9685::get_off_count(ind idx)`, `PCA9685::get_pwm_value(int idx)`: accessor functions
* `uint16_t channel_updated`: Variable for holding the status of each channel. Since there are only 16 channels, we can use 1 bit per channel, and use logical ORs on bit-shifted values to store information about whether the channel has been updated.

### Libraries

The main library is the DLL from Microchip for the MCP2221A. See <https://www.microchip.com/wwwproducts/en/MCP2221A> and download the `MCP2221A DLL (v2.2.1)` file.

These devices are referenced by internal device parameters called Vendor ID (VID) and Product ID (PID). There isn't much documentation about this `(TODO: Look more of this up)`, but it turns out that the defaults for the MCP2221A is in the datasheet for the DLL:

```
VID: 0x4D8
PID: 0xDD

```

Using these, it's possible to connect to a device.

### Qt Interface

The tool can be interacted with via the Qt interface. There is a slider bar or a text input box, into which the value for each channel can be directly set. There are 7 devices (6 optical phased arrays, and the 1 MEMS) that can be interacted with, with 16 channels for each. For the MEMS, there is only 6 usable channels, with 10 extra as debug.

The user will use the slider to set the percentage value for each PWM. Then, press the Send Data button to upload to the device.

## PCA9685 Functional Description

The PCA9685 device is a 16 channel PWM generating circuit, controlled by I2C. The datasheet can be found at: `https://www.nxp.com/docs/en/data-sheet/PCA9685.pdf`

Device addresses are specified in hardware, and can be seen on page 1 of the schematic, reproduced here:

```
Read/Write  |   Device
---------------------------------------------------
0x80/81     |   Device 1
0x82/83     |   Device 2
0x84/85     |   Device 3
0x86/87     |   Device 4
0x88/89     |   Device 5
0x8A/8B     |   Device 6
0x8C/8D     |   MEMS

```

Below is a description of some relevant registers:

```
Name:   Addr    Description (* indicates default value)
MODE1:  0x00
                Bit 7: (0*) RESTART. Write a 1 to restart the device, it will re-set itself to 0 after reset is complete.
                                    Restart mode re-sets to previously set value, when coming out of SLEEP mode.
                Bit 6: (0*) EXTCLK. Indicates whether the external clock is used (it is not in our board)
                Bit 5: (0*) AI: Auto-increment, allows for every PWM to be set sequentially.
                Bit 4: (1*) SLEEP: Sets into sleep mode. LEDs are off in sleep mode.
                Bit 3-1: (0*) SUB1-3: Indicates whether the device responds to the sub-addresses.
                Bit 0: (1*) ALLCALL: Indicates whether the device responds to the ALLCALL address.
MODE2:  0x01
                Bit 7-5: Read only, reserved.
                Bit 4: (0*) INVRT: Whether the output logic is active high (0) or active low (1).
                Bit 3: (0*) OCH: Outputs change on STOP (0), or on ACK (1).
                Bit 2: (1*) OUTDRV: Outputs have open drain (0) or totem-pole (1)
                Bit 1-0: (00*) OUTNE[1:0]: Determines output state during nOE. 00 is outputs driven to 0, 01 is outputs driven to 1, 1X is output undriven/high impedance.
SUBADR1-3 0x02-0x04
                Sub-addresses allow the device to respond to some specified address (allowing multiple devices to be talked to at the same time)
ALLCALLADDR 0x05
                Specifies the all-call address, allowing all devices to respond to this.
LED#_ON_L 0x6 + #*4     0000 0000*
LED#_ON_H 0x7 + #*4     0000 0000*
LED#_OFF_L 0x8 + #*4    0000 0000*
LED#_OFF_H 0x9 + #*4    0001 0000*
                Specifies the on and off times for the devices. Each spans 2 8-bit registers, granting 16 total bits, but only the 12 LSB are used. Thus, bits 7-5 of the _H registers are reserved, and 3:0 of _H and 7:0 of _L form a single 12-bit value.
                There is a free-running counter that runs continually from 0x0000 to 0x0FFF (0 to 4095).
                The driver turns on when the counter value is equal to the 12-bit ON value, and turns off when the counter value is equal to the 12-bit OFF value. Thus, the phase shift can be set.
                OFF takes priority over ON. Thus, when OFF and ON have the same values, the output is all OFF.
                Another way of permanently OFF is to write LED#_OFF_H[4] to 1.
                For permanently ON, LED#_ON_H[4] must be written 1, and LED#_OFF_H[4] must be written 0.

ALL_LED_ON_L 0x250
ALL_LED_ON_H 0x251
ALL_LED_OFF_L 0x252
ALL_LED_OFF_H 0x253
                LED value for all of the devices at once, if necessary.
PRE_SCALE 0x254+
                Sets the output update frequency. Set it to 0x03 (0b00000011) for maximum frequency. Can only be set when SLEEP bit in the MODE registers is 1.
TestMode 0x255

```

For the board, we have an N-type MOSFET being driven, which have both a series and a pull-down resistor. The proper settings for INVRT and OUTDRV in the MODE2 register is: 0 and 1 respectively, to be active high (no invert), and driven as totem pole (no pull-up for open-drain).

In our software, we have a PCA9685 class, that holds the address of the device, the desired value of the PWM, the desired phase, the actual ON and OFF values corresponding to those values, 

On initialization, for each chip, we will do the following.

1. Write 0b00000110 to address 0x01 (MODE2). This results in the correct settings for INVRT and OUTDRV, as well as setting it so in the nOE state the outputs are high impedance (and thus, the gates are pulled down by the pulldown resistor)
2. Since the default values for the PWMs is to be in the full OFF state, the defaults are okay
3. Write 0b00000001 to address 0x00 (MODE1). This leaves all the registers at the default, but takes the device out of sleep mode (thus enabling the PWM outputs)

Driving the devices is also straightforward.

1. It is desirable to not have all of the devices switch on at the same time (high inrush current from the supplies). So, each channel on a device will have a delay of `n * 4096 / 16` where n is the channel number (i.e. 1/16 of a full counter value). This way, at most 7 devices switch from off to on at the same time, reducing inrush current. Thus, for each channel, the turn-on time is a constant value determined only by the delay.
2. Check the PWM value being written. If 0% or 100%, turn on the applicable bits to enable full ON or full OFF.
3. If not 0% or 100%, the turn off time is specified next. We determine the actual pulse-width required from the PWM percentage, multiplied by the 4096 count value, to determine the length of the pulse. This is then added to the turn-on time previously calculated. This is then modulo divided by 4096, to determine the turn-off time. The reason for the modulo division is that the counter is free running and rolls over to 0 at 4096.
4. The new values can be written immediately after being changed. A class variable keeps track of which channels have been changed (a 1 in the bit indexed by the channel number indicates that it is changed.) When writing, just reset this variable to 0.



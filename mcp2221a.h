#ifndef MCP2221A_H
#define MCP2221A_H

#include <stdint.h>
#include "mcp2221_dll_um.h"

int MCP_init(void* device);

int MCP_checkLibrary(wchar_t* libVer);

int MCP_getDevices();

#endif //MCP_2221A_H
#pragma once

#include <stdint.h>

#define PCA9685_CHANNELS 16
#define PCA9685_COUNTER_WIDTH 4096
#define PCA9685_ALL_ONOFF_BIT 0x1000

#define PCA9685_MODE1_REG 0x00
#define PCA9685_MODE2_REG 0x01
#define PCA9685_PRESCALE_REG 0xFE
#define PCA9685_LED_ON_L_BASE 0x06
#define PCA9685_LED_ON_H_BASE 0x07
#define PCA9685_LED_OFF_L_BASE 0x08
#define PCA9685_LED_OFF_H_BASE 0x09
#define PCA9685_LED_INCR 4

#define PCA9685_LED_ON_ZERO 0x0000
#define PCA9685_LED_OFF_ZERO 0x1000

#define PCA9685_LED_ON_FULL 0x1000
#define PCA9685_LED_OFF_FULL 0x0000


class PCA9685 {
    public:
        PCA9685(int dev_num = 0, uint8_t addr = 0x00);
        void PCA_reset();
        void PCA_setpwm(int channel, int pwm);
        void PCA_clear_update_flag();
        void PCA_print_status();
        uint8_t get_address();
        uint16_t get_channel_updated();
        uint16_t get_on_count(int idx);
        uint16_t get_off_count(int idx);
        int get_pwm_val(int idx);
    
    private:
        int     device_num = -1;
        uint8_t address = 0x00;
        int     pwm_val[PCA9685_CHANNELS] = {0};
        uint16_t    on_count[PCA9685_CHANNELS] = {0};
        uint16_t    off_count[PCA9685_CHANNELS] = {0};
        uint16_t    channel_updated = 0;
};
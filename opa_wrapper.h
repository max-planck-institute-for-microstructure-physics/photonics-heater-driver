#pragma once

#include <QWidget>
#include <QLabel>
#include <QString>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <stdio.h>

#include "pwm_device.h"
#include "pca9685.h"

#define N_DEVICES 7

//enum DEVICES {DEV1, DEV2, DEV3, DEV4, DEV5, DEV6, devMEMS};
inline const QString DEVICE_STRINGS[N_DEVICES] = {
    "Device 1",
    "Device 2",
    "Device 3",
    "Device 4",
    "Device 5",
    "Device 6",
    "MEMS"
};

class OPA_Wrapper : public QWidget {

    Q_OBJECT
    
    public:
        OPA_Wrapper(QWidget *parent = 0);
        void print_results();
        void push_all_values();
        int MCP_init();
        int MCP_checkLibrary(wchar_t* libVer);
        void MCP_getDevices();
        void MCP_PCA_init();
    
    private:
        // Device data
        PCA9685 pca_dev[N_DEVICES];
        void* mcp_dev;
        int addresses[N_DEVICES];
        
        // Qt User interface items
        PWM_Device* pwm_devices[N_DEVICES];
        QWidget* spacer[N_DEVICES-1];
        QWidget* vspacer;
        QPushButton* button;
        QHBoxLayout *hbox;
        QVBoxLayout *vbox;
        
        // Internal functions
        void get_addresses_from_file(const char* path = "addTable.txt");
        void MCP_write(int device_num, unsigned char register_address, unsigned int num_bytes, unsigned char* data);
        void MCP_PCA_pwm_values_push(int device_num);
};

unsigned char get_lsbyte(uint16_t in);
unsigned char get_msbyte(uint16_t in);
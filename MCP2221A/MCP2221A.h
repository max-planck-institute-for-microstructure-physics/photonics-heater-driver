#ifndef MCP2221A_H
#define MCP2221A_H

#include <stdint.h>

int MCP_init(void** device);

int MCP_checkLibrary(wchar_t* libVer);

int MCP_getDevices(void);

int MCP_openDevice_index(int index, void** device);

//int MCP_i2cWrite(PCA* pointerPCA, uint8_t regAddr, uint8_t regData);

#endif